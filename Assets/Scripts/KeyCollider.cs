﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCollider : MonoBehaviour {


    private bool triggerKey = false;
    private bool triggerGate = false;
    public bool gateClicked = false;
    public bool destroyGate = false;
    public int keyHave;
    public GameObject[] keys;
    public GameObject touchedKey;
    public GameObject keyButton;
    public GameObject keysContainer;
    public KeyContainer keyContainer;
	// Use this for initialization
	void Start () {
        keysContainer = GameObject.FindGameObjectWithTag("Finish");
        keyContainer = keysContainer.GetComponent<KeyContainer>();
        triggerKey = false;
        keys[0] = GameObject.FindGameObjectWithTag("KeyImage1");
        keys[1] = GameObject.FindGameObjectWithTag("KeyImage2");
        keys[2] = GameObject.FindGameObjectWithTag("KeyImage3");
        keys[3] = GameObject.FindGameObjectWithTag("KeyImage4");
        keyButton = GameObject.FindGameObjectWithTag("KeyButton");
        keyButton.SetActive(false);
        foreach (GameObject key in keys)
        {
            key.SetActive(false);
        }
        }
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0) && triggerKey == true)
        {
            Destroy(touchedKey);
            keyHave += 1;
            keyButton.SetActive(false);
            foreach (GameObject key in keys)
            {
                if(key.active==false)
                {
                    key.SetActive(true);
                    triggerKey = false;
                    break;
                }
            }
        }
        if(Input.GetMouseButtonDown(0) && triggerGate == true && keyHave!=0)
        {
            Debug.Log("TU");
            keyContainer.keyCount += 1;
            keyHave -= 1;
            foreach (GameObject key in keys)
            {
                if (key.active == true)
                {
                    key.SetActive(false);
                    break;
                }
            }
            if (keyContainer.keyCount > 3)
            {
                destroyGate = true;
                Debug.Log("Destroy");
            }
        }

	}

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Key")
        {
            triggerKey = true;
            touchedKey = col.transform.gameObject;
            keyButton.SetActive(true);
        }

        if(col.tag == "Gate")
        {
            triggerGate = true;
            Debug.Log("DD");
        }
    }
    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Key")
        {
            triggerKey = false;
            keyButton.SetActive(false);
        }
        if (col.tag == "Gate")
        {
            triggerGate = false;
        }
    }
    private void OnTriggerStay(Collider col)
    {
        if (col.tag == "Gate" && destroyGate == true)
        {
            Destroy(col.gameObject);
        }
    }
}
