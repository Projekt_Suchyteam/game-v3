﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeySpawner : MonoBehaviour {
    public GameObject barrel;
    public GameObject key;
	// Use this for initialization
	void Start () {
        int randomSpawnX = Random.Range(50, 550);
        int randomSpawnZ = Random.Range(50, 550);
        Instantiate(barrel, new Vector3(randomSpawnX, 2, randomSpawnZ), gameObject.transform.rotation);
        Instantiate(key, new Vector3(randomSpawnX, 4, randomSpawnZ), gameObject.transform.rotation);
        randomSpawnX = Random.Range(50, 550);
        randomSpawnZ = Random.Range(50, 550);
        Instantiate(barrel, new Vector3(randomSpawnX, 2, randomSpawnZ), gameObject.transform.rotation);
        Instantiate(key, new Vector3(randomSpawnX, 4, randomSpawnZ), gameObject.transform.rotation);
        randomSpawnX = Random.Range(50, 550);
        randomSpawnZ = Random.Range(50, 550);
        Instantiate(barrel, new Vector3(randomSpawnX, 2, randomSpawnZ), gameObject.transform.rotation);
        Instantiate(key, new Vector3(randomSpawnX, 4, randomSpawnZ), gameObject.transform.rotation);
        randomSpawnX = Random.Range(50, 550);
        randomSpawnZ = Random.Range(50, 550);
        Instantiate(barrel, new Vector3(randomSpawnX, 2, randomSpawnZ), gameObject.transform.rotation);
        Instantiate(key, new Vector3(randomSpawnX, 4, randomSpawnZ), gameObject.transform.rotation);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
