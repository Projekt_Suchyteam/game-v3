﻿using UnityEngine;
using UnityEngine.Networking;
public class Moving2 : NetworkBehaviour
{

	void Start()
	{
		if (!isLocalPlayer) {
			Destroy (this);
			return;
		}
	}

	void Update()
	{
		if (isLocalPlayer) {
			KeyCode[] arrowKeys = { KeyCode.UpArrow, KeyCode.DownArrow, KeyCode.RightArrow, KeyCode.LeftArrow };
			foreach (KeyCode arrowKey in arrowKeys) {
				if (!Input.GetKey (arrowKey))
					continue;
				switch (arrowKey) {
				case KeyCode.UpArrow:
					transform.Translate (0, 0, Time.deltaTime);
					break;
				case KeyCode.DownArrow:
					transform.Translate (0, 0, -Time.deltaTime);
					break;
				case KeyCode.RightArrow:
					transform.Translate (Time.deltaTime, 0, 0);
					break;
				case KeyCode.LeftArrow:
					transform.Translate (-Time.deltaTime, 0, 0);
					break;
				}
			}
		}
	}
}