﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

[NetworkSettings(channel=2)] 
public class Moving : NetworkBehaviour
{
	struct CubeState
	{
		public int moveNum;
		public float x;
		public float y;
		public float z;
	}
		
	Queue<KeyCode> pendingMoves;
	CubeState predictedState;
	[SyncVar(hook="OnServerStateChanged")] CubeState serverState;


	[Command(channel=0)] 
	void CmdMoveOnServer(KeyCode arrowKey)
	{
		serverState = Move(serverState, arrowKey);
	}

	void Start()
	{
		if (isLocalPlayer) {
			pendingMoves = new Queue<KeyCode>();

			predictedState = new CubeState
			{
				moveNum = 0,
				x = 0,
				y = 0,
				z = 0
			};
		}
	}

	void Awake()
	{
		InitState();
	}

	[Server]
	void InitState()
	{
		serverState = new CubeState
		{
			moveNum = 0,
			x = 0,
			y = 0,
			z = 0
		};
	}

	void Update()
	{

		if (isLocalPlayer) {
			KeyCode[] arrowKeys = { KeyCode.UpArrow, KeyCode.DownArrow, KeyCode.RightArrow, KeyCode.LeftArrow };
			foreach (KeyCode arrowKey in arrowKeys) {
				if (!Input.GetKey(arrowKey)) continue;
				UpdatePredictedState(); 
				CmdMoveOnServer(arrowKey);
			}
		}
		SyncState();
	}

	void SyncState()
	{
		CubeState stateToRender = isLocalPlayer ? predictedState : serverState;
		transform.position = new Vector3(stateToRender.x, stateToRender.y, stateToRender.z);
	}

	CubeState Move(CubeState previous, KeyCode arrowKey)
	{
		float dx = 0;
		float dy = 0;
		float dz = 0;

		switch (arrowKey)
		{
		case KeyCode.UpArrow:
			dz = Time.deltaTime;
			break;
		case KeyCode.DownArrow:
			dz = -Time.deltaTime;
			break;
		case KeyCode.RightArrow:
			dx = Time.deltaTime;
			break;
		case KeyCode.LeftArrow:
			dx = -Time.deltaTime;
			break;
		}

		return new CubeState
		{
			moveNum = 1 + previous.moveNum,
			x = dx + previous.x,
			y = dy + previous.y,
			z = dz + previous.z
		};
	}

	void UpdatePredictedState()
	{
		predictedState = serverState;
		foreach (KeyCode arrowKey in pendingMoves) {
			predictedState = Move(predictedState, arrowKey);
		}
	}

	void OnServerStateChanged(CubeState newState)
	{
		serverState = newState;
		if (pendingMoves != null) {
			while (pendingMoves.Count > (predictedState.moveNum - serverState.moveNum)) {
				pendingMoves.Dequeue();
			}
			UpdatePredictedState();
		}
	}
}
	