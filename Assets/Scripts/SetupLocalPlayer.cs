﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.Person;

public class SetupLocalPlayer : NetworkBehaviour {
	void Start()
	{
		if (isLocalPlayer)
			GetComponent<Moving2> ().enabled = true;
			GetComponent<PersonCharacter> ().enabled = true;
			GetComponent<PersonUserControl> ().enabled = true;
	}
}