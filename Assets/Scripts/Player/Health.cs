﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

	public float health;
	public float currentHealth;
	public bool isDead;
	private bool key = false;

	Animator animator;

	void Start () {
		animator = GetComponent<Animator> ();
		isDead = false;
		currentHealth = health;
	}

	void Awake(){
		
	}
	
	// Update is called once per frame
	void Update () {
		if (currentHealth <= 0) {
			isDead = true;
		}
		else if (key) {
			animator.SetBool ("Hit", true);
			key = false;
		} else {
			animator.SetBool ("Hit", false);
		}
	}

	public void GetHealth(float value){
		if (!isDead) {
			currentHealth -= value;
			key = true;
		}
	}
}
