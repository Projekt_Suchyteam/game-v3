﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour {

	public float damageAmount = 25f;
	public float timeAttack = 1f;
	private GameObject player;
	private Health health;
	private bool range;
	private float timer;
	private bool key;

	public void Awake(){
		range = false;
		player = GameObject.FindGameObjectWithTag ("Player");

		if (player != null) {
			health = player.GetComponent<Health> ();
		}
	}

	public void Update(){
		timer += Time.deltaTime;
		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			timer = 0f;
			key = true;
		}
		if (range && health.currentHealth > 0 && timer >= timeAttack && timer <= timeAttack + 0.1 && key) {
			health.GetHealth (damageAmount);
			key = false;
		}
	}

	public void OnTriggerEnter(Collider other){
		if (other.gameObject == player)
			range = true;
	}

	public void OnTriggerExit(Collider other){
		if (other.gameObject == player)
			range = false;
	}
}
