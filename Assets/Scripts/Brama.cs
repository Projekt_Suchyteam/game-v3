﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brama : MonoBehaviour {
    public int keyCounter;
    public bool trigger;
    public bool clicked;
	// Use this for initialization
	void Start () {

       trigger = false;
       clicked = false;
    }
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0) && trigger == true)
        {
            keyCounter += 1;
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            trigger = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            trigger = false;
        }
    }
}
